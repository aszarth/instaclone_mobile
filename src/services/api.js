import axios from 'axios';

const api = axios.create({
    // 10.0.3.2 pq to usando genymotion pro localhost da máquina
    // pode ser tbm 10.0.2.2 no androidstudio
    baseURL: 'http://10.0.3.2:3333'
})

export default api;