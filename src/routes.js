import React from 'react';
import { createAppContainer, createStackNavigator } from 'react-navigation';
import { Image } from 'react-native';

import Feed from './pages/Feed/Feed';
import New from './pages/New/New';

import logo from './assets/logo.png';

export default createAppContainer(
  createStackNavigator(
    {
      Feed,
      New,
    },
    {
      initialRouteName: 'Feed',
      // todas as telas:
      defaultNavigationOptions: {
        headerTintColor: '#000', // cor do botão
        headerTitle: <Image style={{ marginHorizontal: 20 }} source={logo} />, // margem dos dois lados pra ficar no meio
        headerBackTitle: null, // sem nada escrito, só o icone voltar
      },
      mode: 'modal', // animação vir de baixo pra cima
    }
  )
);
