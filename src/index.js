import React, {Component} from 'react';

// ignorar warnings
import { YellowBox } from 'react-native';
YellowBox.ignoreWarnings([
    'Unrecognized Websocket'
])

import Routes from './routes';

export default function App() {
    return <Routes />
}