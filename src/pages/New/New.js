import React, { Component } from 'react';
import ImagePicker from 'react-native-image-picker';

import {
  Form,
  SelectButton,
  SelectButtonText,
  Input,
  PreviewImage,
  ShareButton,
  ShareButtonText,
} from "./styles";

import api from '../../services/api';

export default class New extends Component {
  static navigationOptions = {
    headerTitle: 'Nova publicação'
  };

  state = {
    preview: null,
    image: null,
    author: '',
    place: '',
    description: '',
    hashtags: '',
  };

  handleSelectImage = () => {
    ImagePicker.showImagePicker({
      title: 'Selecionar imagem',
    }, upload => {
      if(upload.error) {
        console.log('Error');
      }
      else if(upload.didCancel) {
        console.log('Used canceled');
      }
      else {
        const preview = {
          uri: `data:image/jpeg;base64,${upload.data}`,
        }

        let prefix;
        let ext;
        if(upload.fileName) { // converte img heic pra JPG
          [prefix, ext] = upload.fileName.split('.');
          ext = ext.toLowerCase() == 'heic' ? 'jpg' : ext; // se for formato heic vira jpg
        } else { // iOS quando tira foto direto, nao cria nome
          prefix = new Date().getTime();
          ext = 'jpg';
        }
        const image = {
          uri: upload.uri,
          type: upload.type,
          name: `${prefix}.${ext}`,
        };
      }

      this.setState({ preview, image });
    })
  }

  handleSubmit = async () => {
    const data = new FormData();
    data.append('image', this.state.image);
    data.append('author', this.state.author);
    data.append('place', this.state.place);
    data.append('description', this.state.description);
    data.append('hashtags', this.state.hashtags);

    await api.post('posts', data)

    this.props.navigation.navigate('Feed');
  }

  render() {
    return (
      <Form>
        <SelectButton onPress={this.handleSelectImage}>
          <SelectButtonText>Selecionar imagem</SelectButtonText>
        </SelectButton>

        {
          // condicao && é como se fosse um if() code, react permite isso
          this.state.preview && <PreviewImage source={this.state.preview} />
        }

        <Input
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="Nome do autor"
          placeholderTextColor="#999"
          value={this.state.author}
          onChangeText={author => this.setState({ author })}
        />

        <Input
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="Local da foto"
          placeholderTextColor="#999"
          value={this.state.place}
          onChangeText={place => this.setState({ place })}
        />

        <Input
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="Descrição"
          placeholderTextColor="#999"
          value={this.state.description}
          onChangeText={description => this.setState({ description })}
        />

        <Input
          autoCorrect={false}
          autoCapitalize="none"
          placeholder="Hashtags"
          placeholderTextColor="#999"
          value={this.state.hashtags}
          onChangeText={hashtags => this.setState({ hashtags })}
        />

        <ShareButton onPress={this.handleSubmit}>
          <ShareButtonText>Compartilhar</ShareButtonText>
        </ShareButton>
      </Form>
    );
  }
}
