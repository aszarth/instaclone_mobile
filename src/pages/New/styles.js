import styled from 'styled-components';

export const Form = styled.View`
  flex: 1;
  padding: 20px; /* horizontal */
  padding-top: 30px;
`;

export const SelectButton = styled.TouchableOpacity`
    background-color: #7159c1;
    border-radius: 4;
    height: 42;
    margin-top: 15;

    justify-content: center;
    align-items: center;

    margin-bottom: 15;
`;

export const SelectButtonText = styled.Text`
    font-weight: bold;
    font-size: 16;
    color: #FFF;
`;

export const Input = styled.TextInput`
    margin-top: 2;
    margin-bottom: 2;
    border-radius: 4;
    border-width: 1;
    border-color: #CCC;
    border-style: dashed;
    height: 42;
    justify-content: center;
    align-items: center;
`;

export const PreviewImage = styled.Image`
    width: 100;
    height: 100;
    margin-top: 10;
    align-self: "center";
    border-radius: 4;
`;

export const ShareButton = styled.TouchableOpacity`
    background-color: #7159c1;
    border-radius: 4;
    height: 42;
    margin-top: 15;

    justify-content: center;
    align-items: center;
`;

export const ShareButtonText = styled.Text`
    font-weight: bold;
    font-size: 16;
    color: #FFF;
`;
