import styled from 'styled-components';

export const FeedList = styled.View`
  flex: 1;
`;

export const FeedItem = styled.View`
  flex: 1;
  margin-top: 20;
`;

export const FeedItemHeader = styled.View`
  padding: 0 15px; /*paddingHorizontal: 15px,*/
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;
export const PosterName = styled.Text`
  font-size: 14;
  color: #000;
`;
export const PosterPlace = styled.Text`
  font-size: 12;
  color: #666;
  margin-top: 2;
`;

export const FeedItemImage = styled.Image`
  width: 100%;
  height: 400;
  margin: 15px;
`;

export const FeedItemFooter = styled.View`
  padding: 15px;
`;
export const PostLikes = styled.Text`
  margin-top: 15;
  font-weight: bold;
  color: #000;
`;
export const PostDescription = styled.Text`
  line-height: 18;
  color: #000;
`;
export const PostHashtags = styled.Text`
  color: #7159c1;
`;

export const FeedItemActions = styled.View`
  flex-direction: row;
`;
export const ActionButton = styled.TouchableOpacity`
  margin-right: 8;
`;
