import React, { Component } from 'react';
import io from 'socket.io-client';
import api from '../../services/api';

import camera from '../../assets/camera.png';
import more from '../../assets/more.png';
import like from '../../assets/like.png';
import comment from '../../assets/comment.png';
import send from '../../assets/send.png';

import {
  FeedList,
  FeedItem,
  FeedItemHeader,
  PosterName,
  PosterPlace,
  FeedItemImage,
  FeedItemFooter,
  FeedItemActions,
  ActionButton,
  PostLikes,
  PostDescription,
  PostHashtags,
} from './styles';
import { View, Image, TouchableOpacity, FlatList } from 'react-native';

export default class Feed extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerRight: (
      <TouchableOpacity
        style={{ marginRight: 20 }}
        onPress={() => navigation.navigate('New')}
      >
        <Image source={camera} />
      </TouchableOpacity>
    ),
  });

  state = {
    feed: [],
  };

  async componentDidMount() {
    this.registerToSocket();

    const response = await api.get('posts');
    console.log(response.data);

    this.setState({ feed: response.data });
  }

  registerToSocket = () => {
    const socket = io('http://10.0.3.2:3333');

    // post, like
    socket.on('post', (newPost) => [
      // criando um novo feed, com o post que recebeu em realtime
      // e copiando o que já tinha do post
      this.setState({ feed: [newPost, ...this.state.feed] }),
    ]);

    socket.on('like', (likedPost) => {
      // procurar o post com o msm id que o recebido no socket e alterar os likes
      this.setState({
        // percorrer o feed utilizando map
        feed: this.state.feed.map((post) =>
          // se o id post q estou percorrendo nesse momento
          // for igual ao id do post que recebeu um novo curtir
          // atualizar as informações desse novo post com o likePost se nao, manda como ele ja esta no estado
          post.id == likedPost.id ? likedPost : post
        ),
      });
    });
  };

  handleLike = (id) => {
    api.post(`/posts/${id}/like`);
  };

  render() {
    return (
      <FeedList>
        <FlatList
          data={this.state.feed}
          keyExtractor={(post) => post.id}
          renderItem={({ item }) => (
            <FeedItem>
              <FeedItemHeader>
                <View>
                  <PosterName>{item.author}</PosterName>
                  <PosterPlace>{item.place}</PosterPlace>
                </View>
                <View>
                  <Image source={more} />
                </View>
              </FeedItemHeader>

              <FeedItemImage
                source={{ uri: `http://10.0.3.2:3333/files/${item.image}` }}
              />

              <FeedItemFooter>
                <FeedItemActions>
                  <ActionButton onPress={() => this.handleLike(item.id)}>
                    <Image source={like} />
                  </ActionButton>
                  <ActionButton onPress={() => this.handleLike(item.id)}>
                    <Image source={comment} />
                  </ActionButton>
                  <ActionButton onPress={() => this.handleLike(item.id)}>
                    <Image source={send} />
                  </ActionButton>
                </FeedItemActions>

                <PostLikes>{item.likes} curtidas</PostLikes>
                <PostDescription>{item.description}</PostDescription>
                <PostHashtags>{item.hashtags}</PostHashtags>
              </FeedItemFooter>
            </FeedItem>
          )}
        />
      </FeedList>
    );
  }
}
