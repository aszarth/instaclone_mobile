#InstaClone Mobile

![img 1](https://gitlab.com/aszarth/instaclone_mobile/raw/master/showoff/screenshot-2019-07-01_01.08.38.799.png)
![img 2](https://gitlab.com/aszarth/instaclone_mobile/raw/master/showoff/screenshot-2019-07-01_01.08.49.061.png)

FrontEnd MOBILE clone do instagram

Consultando dados de uma API Rest node (https://gitlab.com/aszarth/instaclone_backend)

Com essa aplicação é possível:

- postar fotos
- ver posts, novos posts são atualizados em tempo real
- dar link e atualizar numeros de likes em tempo real

estilização feita com Styled Components

padronização do código com ESLint com padrões google

informações sobre a arquitetura do sistema em:
diario_arquitetura

informações sobre dependencias baixadas e como rodar o projeto em:
diario_npm
